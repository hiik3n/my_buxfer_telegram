import time
import datetime
import sys
from buxfer_client import *
import telegram_bot
import config
import threading

token = None
delay_s = 60
cur_transaction_list = None
date = None
buxfer_config = {'token': token}


def get_buxfer_config():
    buxfer_config.update({'token': get_token(config.USER_NAME, config.PWD)})
    return buxfer_config


def buxfer_listener():
    global token, cur_transaction_list, date
    while 1:
        try:
            time.sleep(delay_s)
            if token is None:
                # Get new token if not available or valid
                token = get_token(config.USER_NAME, config.PWD)

            # Update token
            buxfer_config.update({'token': token})

            if cur_transaction_list is None:
                try:
                    cur_transaction_list = get_current_transactions_from_file()
                except IOError:
                    logger.info("Fail read list of transaction from pickle file, default is set instead")
                    cur_transaction_list = []
                    update_transaction_local_file(cur_transaction_list)

            if token is not None:

                # Get recent transactions
                today = datetime.datetime.today().strftime('%Y-%m-%d')
                if today != date:
                    date = today
                    today = None
                recent_trans_list = get_recent_transactions(token, start_date=today)

                if recent_trans_list is None:
                    logger.warning("Fail to get recent transactions")
                    token = None
                    continue

                logger.info("Get %d recent transaction" % len(recent_trans_list))

                # Find new transactions based on recent list and current list
                new_trans_list = find_new_transaction(recent_trans_list, cur_transaction_list)

                account_list = None
                for _trans in new_trans_list:
                    # Add info about remain balance
                    # only get account info once in each loop
                    if account_list is None:
                        account_list = get_account_info(token)
                        if account_list is None:
                            logger.warning("Fail to get recent transactions")
                            continue
                    _trans["balance"] = next(item["balance"]
                                             for i, item in enumerate(account_list) if item["id"] == _trans["accountId"])
                    print _trans["balance"]
                    telegram_bot.bot_send_transaction_msg(_trans)
                    break

                # Update local file
                cur_transaction_list.extend(new_trans_list)
                update_transaction_local_file(cur_transaction_list)

            else:
                continue
        except urllib2.URLError as e:
            token = None
            logger.warning(repr(e))


if __name__ == '__main__':

    # Enable logging
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    logger = logging.getLogger(__name__)
    logger.info('HelloWorld')


    # Start buxfer listener thread
    t = threading.Thread(name='buxfer_thread', target=buxfer_listener)
    t.setDaemon(True)
    t.start()

    telegram_bot.bot_send_message("bot %s started" % config.BOT_ID)
    telegram_bot.main()
    telegram_bot.bot_send_message("bot %s end" % config.BOT_ID)
    sys.exit(0)
