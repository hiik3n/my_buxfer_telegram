import urllib2
import simplejson
import config
import pickle
import logging


username = config.USER_NAME
password = config.PWD

#############

BASE = "https://www.buxfer.com/api"

INCOME = "income"
EXPENSE = "expense"
DEFINED_TAGS = {'lamchualay': '%23lam%20%23chualay',
                'hungchualay': '%23hung%20%23chualay',
                'nguyenchualay': '%23nguyen%20%23chualay',
                'mongdu': '%23nguyen%20%23chualay'}
TRANSACTION_TYPE = [INCOME, EXPENSE]


class Transaction(object):

    def __init__(self, data):
        self.type = data.get('type', EXPENSE)
        self.description = data.get('des', None)
        self.tags = data.get('tag', None)
        if self.tags is not None:
            if self.tags in DEFINED_TAGS.keys():
                self.tags = DEFINED_TAGS.get(self.tags)
        self.amount = data.get('amount', None)

    def to_http_params(self):
        _list = ['']
        [_list.append('%s=%s' % (item, self.__getattribute__(item)))
         for item in self.__dict__.keys() if self.__getattribute__(item) is not None]
        return "&".join(_list)


get_ids = lambda x: [y['id'] for y in x]

logger = logging.getLogger(__name__)


def save_to_pickle_file(i_obj, i_name='db'):
    with open('%s.pickle' % i_name, 'w') as handle:
        pickle.dump(i_obj, handle, protocol=pickle.HIGHEST_PROTOCOL)


def get_from_pickle_file(i_name='db'):
    with open('%s.pickle' % i_name, 'r') as handle:
        return pickle.load(handle)


def check_error(response):
    result = simplejson.load(response)
    response = result['response']
    if response['status'] != "OK":
        logger.error("An error occured: %s" % response['status'].replace('ERROR: ', ''))
        return None
    return response


def request_api(i_url):
    _req = urllib2.Request(url=i_url)
    return check_error(urllib2.urlopen(_req))


def get_token(user, pwd):
    _url = BASE + "/login?userid=" + user + "&password=" + pwd
    logger.info('Getting token...')
    _response = request_api(_url)
    if _response is None:
        return None
    logger.info('Get token: %s' % _response['token'])
    return _response['token']


def get_recent_transactions(i_token, start_date=None):
    _url = BASE + '/transactions?token=' + i_token
    if start_date is not None:
        _url = _url + '&startDate=' + start_date
    logger.info('Getting recent transactions (%s)...' % _url)
    _response = request_api(_url)
    logger.debug('Get %d transactions' % len(_response['transactions']))
    if _response is None:
        return None
    return _response['transactions']


def convert_text_to_transaction_params(text):
    _items = text.split(',')
    _temp_dict = {}
    for _item in _items:
        _key, _val = _item.strip().rstrip().split('=')
        _temp_dict.update({_key.strip().rstrip(): _val.strip().rstrip().replace(' ', '%20')
                          .replace('#', '%23').replace('\'', '')})
    _transaction_params = Transaction(_temp_dict).to_http_params()
    return _transaction_params


def create_new_transaction(i_token, data):
    _url = BASE + '/add_transaction?token=' + i_token
    _transaction_params = convert_text_to_transaction_params(data)
    logger.info('Create new transaction (%s)...' % (_url +_transaction_params))
    _response = request_api(_url + _transaction_params)
    if _response is None:
        return None
    return repr(_response)


def get_account_info(i_token):
    _url = BASE + '/accounts?token=' + i_token
    logger.info('Getting accounts (%s)...' % _url)
    _response = request_api(_url)
    logger.debug('Get %d account' % len(_response['accounts']))
    if _response is None:
        return None
    return _response['accounts']


def find_new_transaction(recent_list, current_list):
    _res = []
    for _id in set(get_ids(recent_list)) - set(get_ids(current_list)):
        for index, item in enumerate(recent_list):
            if item["id"] == _id:
                _res.append(recent_list[index])
                break
    return _res


def update_transaction_local_file(i_list):
    save_to_pickle_file(i_list, i_name='transactions')


def get_current_transactions_from_file():
    return get_from_pickle_file(i_name='transactions')


if __name__ == '__main__':
    print 'hello world'
    create_new_transaction('1cn39ch4bvf78b1ioj3khmkok4', 'des=abc, amount=1, type=income, tag=abc')

