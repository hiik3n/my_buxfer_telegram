Frequently check if there is any new transaction in Buxfer, there will be any notification 
to others by Telegram.

# Tasks

* [x] Buxfer
    * [x] Get Buxfer token
    * [x] Get transaction list
    * [x] Save transaction list to pickle file
    * [x] Identify new transaction
    * [ ] Add new transaction
    
* [x] Push notification
    * [x] Define data format
    * [x] Telegram bot
        * [x] Create new bot + token
        * [x] Push message to bot

* Future:
    receive cmd from bot to add transaction
    

### SOF

- AttributeError: 'module' object has no attribute 'main'

    ./pip install --upgrade pip==9.0.3 from within the virtualenv/bin directory
    
## Links
    
    - https://www.buxfer.com/help/api
