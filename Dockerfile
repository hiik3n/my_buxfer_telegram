FROM python:2.7

COPY . /app
WORKDIR /app

ENV DEP_FILE requirements.txt

RUN pip install --upgrade pip \
     && pip install -r $DEP_FILE

CMD ["python", "-u", "main.py"]
