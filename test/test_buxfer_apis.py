import pytest
from buxfer_client import get_token, create_new_transaction, convert_text_to_transaction_params
import config


@pytest.fixture(scope='module')
def connector():
    token = get_token(config.USER_NAME, config.PWD)
    return token


@pytest.mark.parametrize("text, expect", [

    ('des=abc, amount=1, type=income, tag=abc', '&description=abc&amount=1&type=income&tags=abc'),

    ("des=abc, amount=10, tag='#abc #123'", '&description=abc&amount=10&type=expense&tags=%23abc%20%23123'),

    ("des=abc, amount=10", '&description=abc&amount=10&type=expense'),

    ("des=abc, amount=10, tag=lamchualay", '&description=abc&amount=10&type=expense&tags=%23lam%20%23chualay')

    ])
def test_convert_text_to_transaction_params(text, expect):
    _params = convert_text_to_transaction_params(text)
    assert set(_params.split('&')) == set(expect.split('&'))


@pytest.mark.parametrize("text, expect", [

    ('des=abc, amount=1, type=income, tag=abc', '&description=abc&amount=1&type=income&tags=abc'),

    ("des=abc, amount=10, tag='#abc #123'", '&description=abc&amount=10&type=expense&tags=%23abc%20%23123'),

    ("des=abc, amount=10", '&description=abc&amount=10&type=expense'),

    ("des=abc, amount=10, tag=lamchualay", '&description=abc&amount=10&type=expense&tags=%23lam%20%23chualay')

    ])
def test_create_new_transaction(connector, text, expect):
    _ret = create_new_transaction(connector, text)
    assert _ret is not None
