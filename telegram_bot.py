from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import config
import traceback
from telegram import Bot
from main import get_buxfer_config
from buxfer_client import create_new_transaction

logger = logging.getLogger(__name__)

myBot = Bot(token=config.BOT_TOKEN)


def bot_send_message(msg):
    myBot.send_message(chat_id=config.BOT_ID, text=msg, parse_mode="Markdown")

# transaction_message = {
#     "description": "#pizza ",
#     "normalizedDate": "2018-03-24",
#     "transactionType": "expense",
#     "amount": 250000,
#     "accountName": "Hand",
#     "balance": 12345
# }

new_transaction_msg_template = '''\[Buxfer] \[NewTransaction] Type=*%s*, Amount=*%d VND*, Desp=*%s* in \
Account=*%s* with CurBalance=*%d* at Date=*%s*'''


def bot_send_transaction_msg(i_transaction):
    print(new_transaction_msg_template % (i_transaction["transactionType"].encode('utf-8'),
                                          i_transaction["amount"],
                                          i_transaction["description"].encode('utf-8'),
                                          i_transaction["accountName"].encode('utf-8'),
                                          i_transaction["balance"],
                                          i_transaction["normalizedDate"].encode('utf-8')))
    bot_send_message(new_transaction_msg_template % (i_transaction["transactionType"].encode('utf-8'),
                                                     i_transaction["amount"],
                                                     i_transaction["description"].encode('utf-8'),
                                                     i_transaction["accountName"].encode('utf-8'),
                                                     i_transaction["balance"],
                                                     i_transaction["normalizedDate"].encode('utf-8')))

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.


def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Bot %s started' % config.BOT_ID)


def help(bot, update):
    """Send a message when the command /help is issued."""
    _help_cmds = ['/buxfer_help', '/buxfer_set']
    update.message.reply_text(repr(_help_cmds))


def buxfer_help(bot, update):
    """Send a message when the command /buxfer_help is issued."""
    _help = '''/buxfer_set des=?, amount=?, type=[expense (default)|income], tag=[?|lamchualay|hungchualay|nguyenchualay]'''
    update.message.reply_text(repr(_help))


def buxfer_set(bot, update):
    """Send a message when the command /buxfer_set is issued."""
    _cmd = '/buxfer_set'
    logger.debug("get %s" % update.message.text)
    _text = update.message.text.replace(_cmd, '').rstrip().strip()
    try:
        _ret = create_new_transaction(get_buxfer_config()['token'], _text)
        update.message.reply_text(repr(_ret))
    except Exception as e:
        update.message.reply_text(traceback.format_exc())


def echo(bot, update):
    """Echo the user message."""
    update.message.reply_text(update.message.text)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(config.BOT_TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("buxfer_set", buxfer_set))
    dp.add_handler(CommandHandler("buxfer_help", buxfer_help))

    # on noncommand i.e message - echo the message on Telegram
    # dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    # main()
    bot_send_message("test test")
